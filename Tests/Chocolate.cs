namespace Tests
{
    public record Chocolate : PayableDrink
    {
        
        public Chocolate(): base(0.5)
        {
           
        }
        public override char ProtocolCode { get { return 'H'; } }
    }
}