namespace Tests
{
    public record SugarCube
    {
        readonly int numberOfSugarCubes;

        public SugarCube(int numberOfSugarCubes)
        {
            this.numberOfSugarCubes = numberOfSugarCubes;
        }

        public override string ToString()
        {
            return   numberOfSugarCubes > 0 ? numberOfSugarCubes.ToString() : string.Empty;
        }
    }
}