using System;

namespace Tests
{
    public class HotOrder : Order
    {
        public HotOrder(IAmDrink drink, int numberOfSugarCubes, Func<double> money) : base(drink, numberOfSugarCubes, money)
        {
        }

        internal override string ProtocoledString()
        {
            if (!drink.CanBeHot)
                return "M:Are you insane?";

            return string.Format("{0}h:{1}:{2}",
                drink.ProtocolCode,
                new SugarCube(numberOfSugarCubes),
                new Stick(numberOfSugarCubes)
            );
        }
    }
}