namespace Tests
{
    public record PayableDrink(double Price) : IAmDrink
    {
        public virtual char ProtocolCode => ' ';

        public virtual double MoneyLeftAfterBuyingMe(double money)
        {            
            return money - Price;
        }

        public virtual bool CanBeHot { get {return true;} }
    }
}