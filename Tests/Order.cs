using System;

namespace Tests
{
    public class Order
    {
        protected readonly IAmDrink drink;
        protected readonly int numberOfSugarCubes;
        protected readonly Func<double> money;

        public Order(IAmDrink drink, int numberOfSugarCubes, Func<double> money)
        {
            this.drink = drink;
            this.numberOfSugarCubes = numberOfSugarCubes;
            this.money = money;
        }

        public Order WithSugar(int numberOfSugarCubes)
        {
            return new Order(drink, numberOfSugarCubes, money);
        }

        internal string Please()
        {
            if (drink.MoneyLeftAfterBuyingMe(this.money()) < 0)
                return "M:message-content";

            return ProtocoledString();
        }

        internal virtual string ProtocoledString()
        {
            return string.Format("{0}:{1}:{2}",
                drink.ProtocolCode,
                new SugarCube(numberOfSugarCubes),
                new Stick(numberOfSugarCubes)
                 );
        }

        internal Order ExtraHot()
        {
            return new HotOrder( drink,  numberOfSugarCubes,  money);
        }
    }
}