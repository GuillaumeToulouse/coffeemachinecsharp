using NUnit.Framework;

namespace Tests
{
    public class DrinkMakerShall
    {
        [Test]
        public void NotMakeTeaWithoutMoeny()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var tea = new Tea();
            //ACT
            sut.insertCoin(0);
            var actual = sut
                .MakeMeA(tea)
                .WithSugar(1)
                .Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("M:message-content"));
        }



        [Test]
        public void MakeTea()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var tea = new Tea();
            //ACT
            sut.insertCoin(0.4);
            var actual = sut
                .MakeMeA(tea)
                .WithSugar(1)
                .Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("T:1:0"));
        }

        [Test]
        public void MakeTeaExtraHot()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var tea = new Tea();
            //ACT
            sut.insertCoin(0.4);
            var actual = sut.MakeMeA(tea, 1).ExtraHot().Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("Th:1:0"));
        }

        [Test]
        public void MakeChocolate()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            //ACT
            sut.insertCoin(1);
            var actual = sut.MakeMeA(new Chocolate(), 0).ExtraHot().Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("Hh::"));
        }

        [Test]
        public void MakeCoffe()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var cof = new Coffee();
            //ACT
            sut.insertCoin(1);
            var actual = sut.MakeMeA(cof, 2).ExtraHot().Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("Ch:2:0"));
        }

        [Test]
        public void MakeOrangeJuice()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var cof = new OrangeJuice();
            //ACT
            sut.insertCoin(1);
            var actual = sut.MakeMeA(cof, 0).Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("O::"));
        }

        [Test]
        public void CannotMakeOrangeJuiceHot()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            //ACT
            sut.insertCoin(1);
            var actual = sut.MakeMeA(new OrangeJuice(), 0).ExtraHot().Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("M:Are you insane?"));
        }

        [Test]
        public void MakeTeaExtraHotAndInsterMoneyAfter()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var tea = new Tea();
            //ACT
            var order = sut
                .MakeMeA(tea)
                .WithSugar(1)
                .ExtraHot();
            sut.insertCoin(0.2);
            sut.insertCoin(0.2);
            //there's no more connascence of execution time; you can add coins before or after making an order
            //but the best to do this is to use immutablity ;)            
            var actual = order.Please();
            //ASSERT
            Assert.That(actual, Is.EqualTo("Th:1:0"));
        }

        [Test]
        public void ReportItsSales()
        {
            //ARRANGE
            var sut = new DrinkMaker();
            var tea = new Tea();
            //ACT
            var order = sut
                .MakeMeA(tea)!
                .WithSugar(1)
                .ExtraHot();
            sut.insertCoin(1);
             order.Please();
            
            
            //ASSERT
            Assert.That( sut.ReportSales().TotalCash, Is.EqualTo(0.4d));
        }
    }
}