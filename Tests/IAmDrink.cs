namespace Tests
{
    public interface IAmDrink
    {
        char ProtocolCode { get; }
        bool CanBeHot { get ; }

        double MoneyLeftAfterBuyingMe(double money);
    }
}