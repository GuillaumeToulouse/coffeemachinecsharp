namespace Tests
{
    public record Stick
    {
        readonly int numberOfSugarCubes;

        public Stick(int numberOfSugarCubes)
        {
            this.numberOfSugarCubes = numberOfSugarCubes;
        }

        public override string ToString()
        {
                return  numberOfSugarCubes>0 ? "0": string.Empty;
        }
    }
}