namespace Tests
{
    public record Tea : PayableDrink
    {
        public Tea() : base(0.4)
        {
        }

        public override char ProtocolCode { get { return 'T'; } }
    }
}