namespace Tests
{
    internal record OrangeJuice : PayableDrink
    {
        public OrangeJuice(): base(0.6)
        {
        }

        public override bool CanBeHot { get {return false;} }

        public override char ProtocolCode => 'O';
    }
}