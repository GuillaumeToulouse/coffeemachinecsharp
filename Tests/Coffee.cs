namespace Tests
{
    public record Coffee : PayableDrink
    {
        public Coffee(): base(0.6)
        {
            
        }
        public override char ProtocolCode => 'C';
    }
}