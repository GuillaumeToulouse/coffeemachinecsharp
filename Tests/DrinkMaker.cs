using System;

namespace Tests
{
    public class DrinkMaker
    {
         double Money { get;  set; }

        public void insertCoin(double money)
        {
            this.Money += money;
        }

        public double GiveMeMonney()
        {
            return Money;
        }



        public Order MakeMeA(IAmDrink drink, int numberOfSugarCubes = 0)
        {
            return new Order(drink, numberOfSugarCubes, GiveMeMonney);
        }

        internal dynamic ReportSales()
        {
            return new Report(TotalCash: 0.4d);
        }
    }
}